APP.component.Header = ClassAvanti.extend({
  init(options) {
    this.setup(options)
    this.start()
    this.bind()
  },

  setup(options) {
    if (typeof APP.i.Helpers === 'undefined') {
      throw new TypeError('You need Helpers Component installed.')
    }

    this.options = $.extend(
      {
        $header: $('header'),
        helpers: APP.i.Helpers,
        classHeaderThin: 'header--thin',
        classMenu: 'header__menu',
        classMenuOpen: 'header__menu--open',
        classMenuLinkSubmenu: 'menu__link--submenu',
        classMenuSubmenu: 'menu__submenu',
        classSubmenuLink: 'submenu__link',
        classMenuLinkOpen: 'menu__link--open',
        classMenuBar: 'menu-bar',
        classMenuBarLink: 'menu-bar__link',
        classMenuBarLinkOpen: 'menu-bar__link--open',
        classSignIn: 'header-account-login',
        classSignOut: 'header-account-logout',
        classSubMenu: 'submenu__item'
      },
      options
    )

    APP.i.Menu = new APP.component.Menu()
    APP.i.Search = new APP.component.Search()
    APP.i.Minicart = new APP.component.Minicart()
  },

  start() {
    this.setLoggedIn()
  },

  setLoggedIn() {
    const { classSignIn, classSignOut } = this.options

    if (!vtexjs.checkout.orderForm) {
      vtexjs.checkout.getOrderForm().then(() => {
        if (vtexjs.checkout.orderForm.loggedIn) {
          $(`.${classSignOut}`).removeClass('hidden')
        } else {
          $(`.${classSignIn}`).removeClass('hidden')
        }
      })

      return true
    }
  },

  bind() {
    this.bindLogin()
    this.bindResponsiveMenu()
    this.bindSubmenu()
    // this.bindSubmenuColumn()
    // this.bindStickyHeader()
    this.bindCloseMenuMobile()
  },

  bindLogin() {
    $(`.${this.options.classSignIn}`).on('click', event => {
      event.preventDefault()

      $('#login').trigger('click')
    })
  },

  bindResponsiveMenu() {
    const $headerMenu = $(`.${this.options.classMenu}`)

    $(`.${this.options.classMenuBarLink}`).on('click', event => {
      event.preventDefault()
      const _this = $(event.currentTarget)

      _this.toggleClass(this.options.classMenuBarLinkOpen)
      $('body').toggleClass('body-lock')
      $headerMenu.toggleClass(this.options.classMenuOpen)
      $('.header__responsive').find('.menu-bar').toggleClass('menu-bar--opened')
      $('.header__responsive').find('.menu-bar__link').toggleClass(this.options.classMenuBarLinkOpen)
    })
  },

  bindSubmenu() {
    if ($(window).width() < 992) {
      $(`.${this.options.classMenuLinkSubmenu}`).on('click', event => {
        event.preventDefault()
        $(event.currentTarget).parent().toggleClass('menu__item--submenu-open')
      })

      // $('body').on('click', `.${this.options.classSubMenu}`, event => {
      //   event.preventDefault()

      //   $(`.${this.options.classSubMenu}`).find('ul').slideUp('fast')
      //   $(event.currentTarget).find('a').toggleClass('menu__link--open')
      // })
    }
  },

  // bindSubmenuColumn() {
  //   if ($(`.${this.options.classMenuBar}`).is(':visible')) {
  //     return true
  //   }

  //   $(`.${this.options.classMenuSubmenu}`).each((index, element) => {
  //     const _this = $(element)
  //     const items = _this.find(`.${this.options.classSubmenuLink}:not(.av-hidden-md.av-hidden-lg)`)

  //     if (items.length > 5) {
  //       _this.addClass('menu__submenu--hasColumn')
  //     }
  //   })
  // },

  // bindStickyHeader() {
  //   const menuWrapper = $('.header__menu')
  //   window.onscroll = function () {
  //     if (this.oldScroll > this.scrollY) {
  //       menuWrapper.addClass('header__menu--visible')
  //     } else {
  //       menuWrapper.removeClass('header__menu--visible')
  //     }
  //     this.oldScroll = this.scrollY
  //   }
  // },

  bindCloseMenuMobile() {
    $('.header__responsive-close').on('click', function(){
      $('.menu-bar__link--open').click();
    })
  }
})
