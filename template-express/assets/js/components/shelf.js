APP.component.Shelf = ClassAvanti.extend({
  init() {
    this.setup()
    this.start()
    this.bind()
  },

  setup() {
    this.buyButton = '.shelf-item__btn-buy',
    this.controlButton = '.shelf-item__qtd-addcart .minicart-control'
  },

  start() {
    this.eachShelf()
  },

  bind() {
    this.handleAddMinicart()
    this.handleProductQuantity()
  },

  eachShelf() {
    $('.shelf-item:not(.shelf-item--initialized)').each((index, element) => {
      const _this = $(element)

      const $flagDiscountPercent = _this.find('.flag-percentage')
      const discountPercent = $flagDiscountPercent.text().trim()

      if(window.innerWidth < 992) {
        if(_this.find('.shelf-item__title-link').text().length > 12) {
          _this.find('.shelf-item__title-link').html(_this.find('.shelf-item__title-link').text().substring(0,12) + '...')
        }
      } else {
        if(_this.find('.shelf-item__title-link').text().length > 30) {
          _this.find('.shelf-item__title-link').html(_this.find('.shelf-item__title-link').text().substring(0,30) + '...')
        }
      }

      if (discountPercent !== '0') {
        const discountFormated = discountPercent.replace(/^(\d+),(\d+)\s(%)$/g, '$1$3')
        $flagDiscountPercent.html('<strong>'+discountFormated+'</strong><span>OFF</span>').addClass('flag-percentage--active')
      }

      _this.addClass('shelf-item--initialized')
    })
  },

  handleAddMinicart() {
    $('body').on('click', this.buyButton, e => {
      e.preventDefault()
      let skuId = 0

      $(e.target).closest('.shelf-item').addClass('loading')

      vtexjs.catalog.getProductWithVariations($(e.target).closest('.shelf-item').attr('data-product-id')).done(function(product) {
        skuId =
          product &&
          product.skus.find(sku => {
            return sku.available
          }).sku

        var item = {
          id: skuId,
          quantity: parseInt($(e.target).closest('.shelf-item').find('.minicart-control--items').html()),
          seller: '1'
        }

        vtexjs.checkout.addToCart([item], null, 1).done(function(orderForm) {
          $(e.target).closest('.shelf-item').removeClass('loading')
          APP.i.Minicart.populateCart()
          APP.i.Minicart.openCart()
        })
      })
    })
  },

  handleProductQuantity() {
    $('body').on('click', this.controlButton, e => {
      e.preventDefault()

      let qtd = parseInt($(e.target).parent().find('.minicart-control--items').html())

      if($(e.target).hasClass('minicart-control--less')) {
        if (qtd === 1) {
          return false
        }
  
        $(e.target).parent().find('.minicart-control--items').html(qtd - 1)
      } else if($(e.target).hasClass('minicart-control--more')) {
        $(e.target).parent().find('.minicart-control--items').html(qtd + 1)
      }
    })
  }
})
