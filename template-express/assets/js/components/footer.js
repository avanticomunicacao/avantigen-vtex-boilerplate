APP.component.Footer = ClassAvanti.extend({
  init(options) {
    this.setup(options)
    this.start()
    this.bind()
  },

  setup(options) {

    APP.i.Newsletter = new APP.component.Newsletter({
      onSuccess() {
        $('.footer-newsletter .newsletter__input').val('')

        alert('Cadastro efetuado com sucesso!')
      }
    })
  },

  start() { },

  bind() { }
})
