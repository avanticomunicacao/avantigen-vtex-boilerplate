APP.component.Menu = ClassAvanti.extend({
  init(options) {
    this.setup(options)
    this.start()
    this.bind()
  },

  setup(options) {
    if (typeof APP.i.Helpers === 'undefined') {
      throw new TypeError('You need Helpers Component installed.')
    }

    this.options = $.extend(
      {
        helpers: APP.i.Helpers,
        $menu: $('.menu__list'),
        categoryLevel: 2
      },
      options
    )

    this.categoryTree = null
  },

  start() {
    this.getCategoryTree()
    // this.getMarcas()
  },

  getCategoryTree() {
    const { categoryLevel, $menu } = this.options

    let deptsArr = []
    $('.menu__list')
      .find('.menu__item')
      .not('.menu__item--all')
      .not('.menu__item--outlet')
      .each((i, e) => {
        const _this = $(e)
        // const depts = _this.find('> a').text().trim()
        const depts = _this.find('.header-main-menu-drop-list').data('name')
        deptsArr.push(depts)
      })

    let itemArr = []

    $.ajax({
      url: `/api/catalog_system/pub/category/tree/${categoryLevel}/`,
      type: 'GET',
      success: data => {
        data.map(item => {
          const elementAll = `<li class="submenu__item" data-cat="menu__item--${item.url.split('/').slice(-1)}">
            <li class="submenu__item">
              <a href="${item.url}" class="submenu__link">${item.name}</a>
            </li>
            `
          $('.menu__item--all')
            .find('.menu__submenu')
            .append(elementAll)
          // console.log(item.url.split('/').slice(-1))
        })

        deptsArr.filter(dept => {
          // console.log(dept)
          data.map(item => {
            if (item.url.split('/').slice(-1) == dept) {
              itemArr.push(item)
              const name = item.url.split('/').slice(-1)
              // console.log(name)
              const element = `<ul class="submenu__list" data-cat="menu__item--${item.url.split('/').slice(-1)}">
                                
                                ${item.children
                  .map(
                    categories => `
                                <li class="submenu__item">
                                  <a href="${categories.url}" class="submenu__link ${categories.children.length > 0 ? 'menu__link-submenu' : ''}">${categories.name}</a>

                                  <ul class="submenu__list--level">
                                    ${categories.children
                        .map(
                          item => `
                                      <li class="submenu__item--level">
                                        <a href="${item.url}" class="submenu__link--level">${item.name}</a>
                                      </li>
                                    `
                        )
                        .join('')}
                                  </ul>
                                </li>
                              `
                  )
                  .join('')}
                              <li class="submenu__item submenu__item--ver-tudo" ><a href="/${name}" class="submenu__link">Ver tudo</a></li>
                              </ul>`
              $menu.find(`[rel="${name}"]`).append(element)
            }
          })
        })
      }
    })
  },

  getMarcas () {
    $('.menu__item-marcas').find('.header-main-menu-drop-list').prepend('<ul class="submenu__list"><li class="submenu__item" data-index="" data-id=""><a href="/busca" class="submenu__link">Ver todas</a></li></ul>')

    const brandsIds = $('.header-main-menu-marcas').text().split(',').reverse();
    console.log(brandsIds)
    let indexVal = 0
    brandsIds.map(marca => {
      const url = `/api/catalog_system/pub/brand/${marca.trim()}`
      const type = 'GET'
      $.ajax({
        url,
        type,
        headers: {
          'Accept': 'application/vnd.vtex.ds.v10+json',
          'Content-Type': 'application/json'
        }
      }).then(result => {
        // console.log(result)
        this.buildMarcassMenu(result, indexVal++)
      }, error => {
        throw new Error('NÃ£o foi possivel localizar a marca')
      })
    })
  },

  buildMarcassMenu (result, indexVal) {
    const name = result.name // this._removeCharacters(result.name)
    const link = name.normalize("NFD")//.replace(/[^a-zA-Zs]/g, '-')
    const id = result.id

    const marcaEl = `<li class="submenu__item" data-index="${indexVal}" data-id="${id}">
                          <a href="/${link}" class="submenu__link">${name}</a>
                        </li>`

    // $(marcaEl).insertBefore($('.submenu__item--see-more'))
    $(marcaEl).prependTo($('.menu__item-marcas .submenu__list'))
  },

  // _removeCharacters (str) {
  //   str = str.toLowerCase().trim()

  //   // remove accents
  //   const from = "Ã¥Ã Ã¡Ã£Ã¤Ã¢Ã¨Ã©Ã«ÃªÃ¬Ã­Ã¯Ã®Ã²Ã³Ã¶Ã´Ã¹ÃºÃ¼Ã»Ã±Ã§"
  //   const to = "aaaaaaeeeeiiiioooouuuunc"

  //   for (let i = 0; i < from.length; i++) {
  //     str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i))
  //   }

  //   return str
  // },

  bind() { }
})
