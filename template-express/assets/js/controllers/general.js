import "../../scss/0-<%= storeAcronym %>-web-styles.scss";

APP.controller.General = ClassAvanti.extend({
  init: function() {
    this.setup()
    this.start()
  },

  setup: function() {
    APP.i.Helpers = new APP.component.Helpers()
    APP.i.Header = new APP.component.Header()
    APP.i.Footer = new APP.component.Footer()
    APP.i.Modal = new APP.component.Modal()
    APP.i.Shelf = new APP.component.Shelf()
  },

  start: function() {
    this.checkIphone()
    this.removeHelperComplement()
    this.checkLoggedIn()
  },

  checkIphone: function() {
    const iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)

    if (iOS) {
      $('html').addClass('iphone')
    }
  },

  removeHelperComplement: function() {
    const interval = setInterval(() => {
      if ($('p.welcome').find('em').length) {
        clearInterval(interval)

        if(!$('p.welcome a[id="login"]').length) {
          $('p.welcome').find('em').remove()
        }
      }
    }, 100)

    $('[id^="helperComplement_"]').remove()
  },

  _registerSlickIntervalBind(callback) {
    const interval = setInterval(() => {
      const resize = $._data(window, 'events').resize

      if (typeof resize === 'undefined') {
        return false
      }

      if (resize[0].namespace === '') {
        clearInterval(interval)

        callback()
      }
    }, 100)
  },

  checkLoggedIn () {
    const intervalVtexjs = setInterval(() => {
      if (vtexjs.checkout.orderForm) {
        clearInterval(intervalVtexjs)
        
        if (vtexjs.checkout.orderForm.loggedIn) {
          $('.header__account').addClass('logado')
        }
      }
    }, 100)
  }
})
