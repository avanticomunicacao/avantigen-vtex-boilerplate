APP.controller.Institucional = ClassAvanti.extend({
  init() {
    this.setup()
    this.start()
    this.bind()
  },

  setup() {
    APP.i.InstitutionalForm = new APP.component.InstitutionalForm()

    this.sidebar = $('.sidebar')
    this.sidebarButton = this.sidebar.find('.sidebar-btn-action')
    this.sidebarContent = this.sidebar.find('.sidebar__content')
  },

  start() {
    this.institucionalSidebar()
  },

  institucionalSidebar() {
    if ($('body').hasClass('institucional')) {
      var urlHash = window.location.href
      var urlClean = urlHash.split('.com.br')[1]
    }

    if (urlClean != null) {
      $('.sidebar__list')
        .find('li a')
        .each(function() {
          $(this).removeClass('selected')
          if ($(this).attr('href') == urlClean) {
            $(this).addClass('selected')
          }
        })
    }
  },

  bind() {
    this._bindSidebarOpen()
    this.breadcrumbName()
  },

  _bindSidebarOpen() {
    this.sidebarButton.on('click', () => {
      this.sidebarContent.toggleClass('sidebar__content--open')
    })
  },

  breadcrumbName() {
    $('.breadcrumb')
      .find('ul')
      .append('<li typeof="v:Breadcrumb">Institucional</li>')
  }
})
