APP.controller.Manufacturers = ClassAvanti.extend({
  init: function() {
    this.setup()
    this.start()
    this.bind()
  },

  setup() {
    this.options = {
     
    }
  },

  start() {
    $('.manufacturers-list').each(function() {
      var letter = $(this).find('.manufacturers-list__letter').html()

      if($(this).find('li').length == 1) {
        $('.letter-item[href="#letter-'+letter.toLocaleLowerCase()+'"]').addClass('unavailable')
      }
    });
  },

  bind() {
    $(document).on('click', '.letter-item', function (event) {
      event.preventDefault();
  
      $('html, body').animate({
          scrollTop: $($.attr(this, 'href')).offset().top - 200
      }, 500);
  });
  }
})
