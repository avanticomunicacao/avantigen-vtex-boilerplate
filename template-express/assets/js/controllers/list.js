window.goToTopPage = function () { }
$(function () {
  window.goToTopPage = function () { }
})

APP.controller.List = ClassAvanti.extend({
  init: function () {
    this.setup()
    this.start()
    this.bind()
  },

  setup: function () {
    this.$mobIsVisible = $('.menu-bar')
    this.$header = $('.header')
    this.$mainShelf = $('.main-shelf')
    this.$searchFilter = $('.search-filter')

    this.$resultItems = $('.resultItemsWrapper div[id^="ResultItems"]')
    this.$loading = $('.search-default__loading')
    this.$noResult = $('.search-default__no-result')

    this.$order = $('#search-order')
    this.mainShelfPosition = parseInt(this.$mainShelf.offset().top) - this.$header.outerHeight();

    APP.i.FilterColors = new APP.component.FilterColors()
    if($(window).width() < 992) {
      APP.i.Select = new APP.component.Select({
        selector: this.$order
      })
    }
  },

  start: function () {
    this.setTamanhos();
    this.createClearFilterButton();
    this.removeFilterCounter();
    this.createHeaderFilter();
    this.changeFilterOrder();
    this.startAvantiSearch();
    this.bannerMarcas();
  },

  setTamanhos: function() {
    $('.search-multiple-navigator fieldset').find('h5').each((i, e) => {
      const _this = $(e)
      if(_this.text() == 'Tamanho') {
        _this.closest('fieldset').addClass('Tamanho')
      }
    })
  },

  createClearFilterButton: function () {
    const $buttons = `<div class="search-filter__buttons">
                        <button class="search-filter__button search-filter__button--apply">Aplicar filtros</button>
                        <button class="search-filter__button search-filter__button--clear">Limpar filtros</button>
                      </div>`

    this.$searchFilter.append($buttons)
  },

  removeFilterCounter: function () {
    const self = this;

    $('.menu-departamento label, .menu-departamento a').each(function () {
      const _this = $(this);

      const text = _this.text();
      const removeCounter = text.replace(/(\s)(\()([0-9]*)(\))/gi, '');

      if (typeof _this.contents()[1] !== 'undefined') {
        _this.contents()[1].nodeValue = removeCounter;
      } else {
        _this.text(removeCounter);
      }
    });
  },

  createHeaderFilter: function () {
    const $target = $('.search-single-navigator')

    const $headeFilter = `<div class="filter__header">
                            <div class="menu__header-title">Filtros</div>

                            <a href="#" class="filter__header-close">
                              <span class="filter__header-bars"></span>
                            </a>
                          </div>`

    $target.prepend($headeFilter)

    if ($('body').hasClass('search')) {
      $('.menu__header-title').html('Categorias')
    }
  },

  changeFilterOrder: function () {
    $('.search-single-navigator').after($('.search-multiple-navigator'))
  },

  startAvantiSearch: function () {
    const {
      $resultItems,
      $loading,
      $noResult,
      $order
    } = this

    $('body').on('avantisearch.emptySearch', () => {
      $loading.hide()
      $noResult.show()
    })

    /**
     * Init Without Cookie
     */
    $resultItems.on('avantisearch.initWithoutCookie', () => {
      $loading.hide()

      setTimeout(() => {
        $('.main-shelf > .main-shelf').show()
      }, 300)
    })

    /**
     * Start
     */
    $resultItems.avantiSearch({
      $selectOrder: $order,
      textLoadLess: 'CARREGAR PRODUTOS ANTERIORES',
      textLoadMore: 'CARREGAR MAIS PRODUTOS',
      pagination: false,
      defaultParams: {
        'query': {
          'O': 'OrderByReleaseDateDESC'
        }
      }
    })

    /**
     * Before Filter and Before Change Order
     */
    $resultItems.on('avantisearch.beforeFilter avantisearch.beforeChangeOrder', (event, options) => {
      const {
        $result,
        classLoadLess,
        classLoadMore
      } = options

      const $item = $result.find('.main-shelf > ul')
      const $buttons = $(`.${classLoadLess}, .${classLoadMore}`)

      const itemPosition = parseInt($item.offset().top, 10) - 180

      $loading.show()
      $noResult.hide()

      this._buttonsToggle($buttons, 'hide')
      this._slideToggle($result, 'slideUp')
      this._scrollTop(itemPosition)
    })

    /**
     * After Filter and After Change Order
     */
    $resultItems.on('avantisearch.afterFilter avantisearch.afterChangeOrder', (event, options) => {
      const {
        $result,
        classLoadLess,
        classLoadMore
      } = options

      const $buttons = $(`.${classLoadLess}, .${classLoadMore}`)

      this._slideToggle($result, 'slideDown')
      this._buttonsToggle($buttons, 'show')
    })

    /**
     * Before Search
     */
    $resultItems.on('avantisearch.beforeSearch', () => {
      $noResult.hide()
    })

    /**
     * After Search
     */
    $resultItems.on('avantisearch.afterSearch', (event, options) => {
      APP.i.Shelf = new APP.component.Shelf()

      if (options.totalItems === 0) {
        $noResult.show()
      }

      $loading.hide()

      setTimeout(() => {
        $('.main-shelf > .main-shelf').show()
      }, 300)
    })
  },

  _buttonsToggle($element, action) {
    $element
      .not('.load-btn--hide')[action]()
  },

  _slideToggle($element, direction) {
    const $list = $element.find('> div > ul')

    $list
      .stop(true, true)[direction]('slow', () => {
        $list.css({
          overflow: 'visible'
        })
      })
  },

  _scrollTop(position) {
    $('html, body')
      .stop()
      .animate({
        scrollTop: position
      }, 500)
  },

  _isMob: function () {
    if (this.$mobIsVisible.is(':visible')) {
      return true
    }

    return false
  },

  bannerMarcas: function () {
    if($('body').hasClass('list')) {
      if($('.listagem-banner').html() == '') {
        $('.listagem-banner').hide();
      }
    }
  },

  bind: function () {
    this.bindOpenFilter()
    this.bindCloseFilter()
    this.bindResetFilter()
    this.bindSearchResults()
  },

  bindOpenFilter: function () {
    $('.button__open-filter').on('click', event => {
      event.preventDefault()

      $('body').addClass('body-lock')
      this.$searchFilter.addClass('opened')
    })
  },

  bindCloseFilter: function () {
    $('.filter__header-close, .search-filter__button--apply').on('click', event => {
      event.preventDefault()

      $('body').removeClass('body-lock')
      this.$searchFilter.removeClass('opened')
    })
  },

  bindResetFilter: function () {
    $('.search-filter__button--clear').on('click', event => {
      event.preventDefault()

      const $departament = $('.menu-departamento')
      const $checked = $departament.find('input[type="checkbox"]:checked')

      $checked.removeAttr('checked')
      $departament.find('label.filter--active').removeClass('filter--active')

      const filters = []
      $checked.each(function () {
        const _this = $(this)

        const filter = _this.attr('rel')

        filters.push(filter)
      })

      AvantiSearch._refreshFilter(filters, false, $(this))
      // this._hideResult()
      this._scrollTop(0)

      if ($('.menu-bar').is(':visible')) {
        $('body').removeClass('body-lock')
        this.$searchFilter.removeClass('opened')
      }
    })
  },

  bindLoading() {
    if ($('.loading-list').length === 0) {
      $('.search__result').find('.load-more').before(`<span class="loading-list"></span>`)
    }
  },

  bindSearchResults() {
    if ($('body').hasClass('search')) {
      // $('.search-result-header').show()
      const searchNumber = $('.resultado-busca-numero').first().find('.value').text()
      const searchTerm = $('.resultado-busca-termo').first().find('.value').text()
      $('.search-result-header').find('span').html(searchNumber)
      $('.search-result-header').find('strong').html(searchTerm)

      if ($(window).width() < 992) {
        const elSearch = $('.search-result-header')
        // $('.search-default').before(elSearch)
        elSearch.appendTo($('.search__result-top'))
      }
    }
  }

})
