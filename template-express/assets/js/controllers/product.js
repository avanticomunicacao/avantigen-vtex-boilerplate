APP.controller.Product = ClassAvanti.extend({
  init: function () {
    var self = this;

    this.setup()
    this.start()
    this.bind()
  },

  setup: function () {
    this.isProductAvailable = skuJson.available;
    this.productBuyInfo = $('.product-buy-info');
    this.buyButton = $('.buy-button');
    this.productPrice = $('.product-price');
    this.productShipping = $('.product-shipping');
    this.productThumbs = $('.apresentacao #show .thumbs');
    this.slickProductsRelated = $('.product-related .main-shelf > ul');
    this.mainImg = $('#include');
  },

  start: function () {
    this.setSkuCorImage()
    this.searchThumbs()
    this.setNotifyMe()
    this.startShipping()
    this.startSlickProductsRelated()
    this.changePrice()
    this.startZoomImgs()
    this.selectedCor()
  },

  setSkuCorImage: function() {
      $('.product-sku .Cor label').each(function() {
          let color = $(this).text();
          
          $(this).html('<img src="/arquivos/thumb-cor_' + APP.i.Helpers._slugify(APP.i.Helpers._removeAccentuation(color)) + '.jpg" />');
      });
  },

  searchThumbs: function () {
    if ($(window).width > 992) {
      const thumbsImages = setInterval(() => {
        const $thumbs = $('.thumbs')
        if ($('.thumbs li').length > 0) {
          clearInterval(thumbsImages)
          if ($thumbs.hasClass('slick-initialized')) {
            $thumbs.slick('unslick')
          }
          $('#show').removeClass('displayed')
          $thumbs.removeClass('displayed')
          this.startSlickThumbs()
        }
      }, 200)
    } else {
      const thumbsImages = setInterval(() => {
        const $thumbs = $('.thumbs')
        const $thumbsItem = $thumbs.children('li').find('img')

        if ($thumbsItem.length > 0) {
          clearInterval(thumbsImages)
          $('#show').removeClass('displayed')
          $thumbs.removeClass('displayed')
          $thumbs.children('li').each((i, e) => {
            const _this = $(e)
            const imgSrc = _this.find('img').attr('src').replace(/-55-55/g, '-800-800')
            _this.find('img').attr('src', imgSrc)
          })
          const thumbsHTML = $thumbs.html()

          if ($thumbs.hasClass('slick-initialized')) {
            $thumbs.slick('unslick')
          }

          setTimeout(() => {
            $thumbs.html(thumbsHTML)
            this.startSlickThumbs()
            clickThumbs() // eslint-disable-line no-undef
          }, 200)
        }
      }, 100)
    }
  },

  startSlickThumbs: function () {
    const thumbsLength = this.productThumbs.find('> li')
    this.productThumbs.removeClass('slick-initialized')
    this.productThumbs.removeClass('slick-slider')
    this.productThumbs.removeClass('slick-vertical')
    if (thumbsLength.length > 1) {
      clickThumbs()
      this.productThumbs.slick({
        autoplay: false,
        infinite: false,
        vertical: true,
        swipe: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        responsive: [
          {
            breakpoint: 992,
            settings: {
              autoplay: false,
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              dots: true,
              vertical: false,
              swipe: true
            }
          }
        ]
      })
    }
    this.changeImgSize()
    $('#show').addClass('displayed')
    $('.thumbs').addClass('displayed')
  },

  changeImgSize: function () {
    const intervalImg = setInterval(() => {
      if ($('#image-main').length > 0) {
        clearInterval(intervalImg)
        const imgSrc = $('#image-main').attr('src').replace('-292-292', '-500-500')
        $('#image-main').attr('src', imgSrc)
        const intervalVisible = setInterval(() => {
          if ($('#image-main').attr('src') === imgSrc) {
            clearInterval(intervalVisible)
            $('#image-main').addClass('displayed')
          }
        }, 200)
      }
    }, 100)
  },

  setNotifyMe: function () {
    if (!this.isProductAvailable) {
      this.productBuyInfo.addClass('product-buy-info--notify-me')
      this.productShipping.hide()
      $('.skuList').each((i, e) => {
        const _this = $(e)
        _this.find('input:first-child, label:first-child').click()
      })
    } else {
      this.productBuyInfo.removeClass('product-buy-info--notify-me')
      this.productShipping.show()
    }

    $('.sku-notifyme-button-ok').attr('value', 'Cadastrar')
    $('.sku-notifyme-form').find('#notifymeClientName').val('Cliente Interessado')
    $('.sku-notifyme-form').find('#notifymeClientEmail').attr('placeholder', 'E-mail')
  },

  startShipping: function () {
    ShippingValue() 
    const changeShippingText = setInterval(() => {
      if ($('.product-shipping label.prefixo').length > 0) {
        clearInterval(changeShippingText)
        $('.product-shipping label.prefixo')[0].childNodes[0].remove()
        $('.product-shipping label.prefixo').find('input[type=text]').attr('placeholder', 'Digite seu cep')

        $('.freight-btn').on('click', () => {
          $(document).ajaxStop(function() {
            $('.freight-values tbody tr').each(function() {
              $(this).find('td').eq(1).html( $(this).find('td').eq(1).html().replace(',', ' - ').replace('para o CEP ' + $('.freight-zip-box').val(), '') )
            });
          });
        })
      }
    }, 200)
  },

  startSlickProductsRelated: function () {
    this.slickProductsRelated.slick({
      dots: true,
      arrows: true,
      slidesToShow: 4,
      slidesToScroll: 4,
      infinite: false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            dots: true,
            arrows: false
          }
        }
      ]
    })
  },

  bind: function () {
    this._bindBuyButton()
    this._bindSkuSelection()
    this._bindChangeThumbs()
  },

  _bindBuyButton: function () {
    this.buyButton.on('click', event => {
      let hrefBuyButton = this.buyButton.attr('href')

      if (hrefBuyButton.indexOf('alert') > -1) {
        return true
      }

      event.preventDefault()

      APP.i.Minicart.addCart(hrefBuyButton, () => {
        APP.i.Minicart.openCart()
      })
    })
  },

  _bindSkuSelection: function () {
    $('.dimension-Cor').on('click', event => {
      const _this = $(event.currentTarget)
      $('#show').removeClass('displayed')
      $('#image-main').removeClass('displayed')
      $('.thumbs').removeClass('displayed')

      if (_this.hasClass('sku-picked')) {
        return
      }

      this.searchThumbs()
      this.setAvailableConditions(_this)
      this.changePrice()
      this.startZoomImgs()
      if($('.sku-notifyme').is(":visible") && !$('.product-buy-info').hasClass('product-buy-info--notify-me')){
        $('.product-buy-info').addClass('product-buy-info--notify-me')
      } 
    })

    $('.dimension-Tamanho').on('click', event => {
      const _this = $(event.currentTarget)
      $('#show').removeClass('displayed')
      $('#image-main').removeClass('displayed')
      $('.thumbs').removeClass('displayed')

      if (_this.hasClass('sku-picked')) {
        return
      }

      this.searchThumbs()
      this.setAvailableConditions(_this)
      this.changePrice()
      this.startZoomImgs()
    })
  },

  _bindChangeThumbs: function () {
    $('body').on('click', '#botaoZoom', () => {
      $('#image-main').removeClass('displayed')
      this.changeImgSize()
      this.startZoomImgs()
    })
  },


  setAvailableConditions: function (element) {
    if (element.hasClass('item_unavailable') && element.hasClass('disabled')) {
      this.productBuyInfo.addClass('product-buy-info--notify-me')
      this.productPrice.hide()
      this.productShipping.hide()
    } else {
      this.productBuyInfo.removeClass('product-buy-info--notify-me')
      this.productPrice.show()
      this.productShipping.show()
    }
  },

  changePrice: function () {
    // if (skuJson.skus[0].listPrice > 0) {
    //   // $('.descricao-preco').addClass('hasListPrice')
    //   const intervalListPrice = setInterval(() => {
    //     if ($('.product-buy-info').find('.valor-de').length > 0) {
    //       clearInterval(intervalListPrice)
    //       const valorDe = $('.valor-de').html().replace('De: ', '')
    //       $('.valor-de').html(valorDe)
    //     }
    //   }, 100)
    // }

    // const intervalBestPrice = setInterval(() => {
    //   if ($('.product-buy-info').find('.valor-por').length > 0) {
    //     clearInterval(intervalBestPrice)

    //     const valorPor = $('.valor-por').html().replace('Por: ', '')
    //     $('.valor-por').html(valorPor);

    //     if ($('.skuBestInstallmentValue')) {
    //       $('.valor-dividido').append(`<span> sem juros</span>`)
    //     }
    //     $('.productPrice').show()
    //   } else if ($(".product-buy-info--notify-me")) {
    //     // Se o produto estiver indisponivel
    //     clearInterval(intervalBestPrice)
    //   }
    // }, 100)

    if ($('.skuBestInstallmentValue')) {
      $('.valor-dividido').append(`<span> sem juros</span>`)
    }
  },

  startZoomImgs: function () {
    jqzoom = () => { }

    const intervalZoom = setInterval(() => {
      if ($(".image-zoom").length > 0) {
        clearInterval(intervalZoom)
        const srcImgSmall = $('#image-main').attr('src')
        const srcImgLarge = $(".image-zoom").attr('href')
        const newImg = `<figure class="zoo-item" data-zoo-image="${srcImgLarge}" data-zoo-scale="2"></figure>`
        $('.image-zoom').html(newImg)
        $('.zoo-item').ZooMove()
      }
    }, 100)
  },

  selectedCor: function () {
    $('.item-dimension-Cor').find('label:not(.item_unavailable)')[0].click();
  },

});
