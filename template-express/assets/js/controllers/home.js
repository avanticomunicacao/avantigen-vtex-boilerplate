APP.controller.Home = ClassAvanti.extend({
  init: function () {
    this.setup()
    this.start()
    this.bind()
  },

  setup() {
    this.options = {
      classSlickMainBanner: 'home-banner-full, .home-banner-full-desktop',
      classSlickShelf: 'main-shelf'
    }
  },

  start() {
    this.startSlickBannerFull()
    this.startSlickBenefits()
    this.startSlickMarcas()
    this.bannersCategorias()
    this.startSlickShelf()
    this.addTargetBlank()
  },

  startSlickBannerFull() {
    const { classSlickMainBanner } = this.options

    const slickOptions = {
      autoplay: true,
      slidesToScroll: 1,
      slidesToShow: 1,
      dots: true,
      arrows: false
    }

    $(`.${classSlickMainBanner}`).on('init', function (event, slick) {
      $(this).parent().removeClass('pre-loading')
    })

    $(`.${classSlickMainBanner}`).slick(slickOptions)
  },

  startSlickBenefits() {
    if ($(window).width() < 992) {
      const slickOptions = {
        autoplay: true,
        slidesToScroll: 1,
        slidesToShow: 1,
        dots: false,
        arrows: false
      }
      $('.benefits-container').slick(slickOptions)
    }
  },

  startSlickMarcas (){
    const slickOptions = {
      autoplay: false,
      slidesToScroll: 8,
      slidesToShow: 8,
      dots: true,
      arrows: true,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
            dots: true,
            arrows: false
          }
        }
      ]
    }
    $('.home-banners-marcas .av-col-xs-24').slick(slickOptions)
  },

  bannersCategorias() {
    $('.home-banners-categorias .box-banner').each(function(){
      let nome = $(this).find('img').attr('alt')
      $(this).find('a').append($('<span>'+nome+'</span>'))
    })

    if ($(window).width() < 1200) {
      this.startSlickCategorias()
    }
  },

  startSlickCategorias (){
    const slickOptions = {
      autoplay: false,
      slidesToScroll: 8,
      slidesToShow: 8,
      dots: true,
      arrows: false,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4
          }
        }
      ]
    }
    $('.home-banners-categorias .av-col-xs-24').slick(slickOptions)
  },

  startSlickShelf() {
    const { classSlickShelf } = this.options

    const slickOptions = {
      autoplay: false,
      slidesToScroll: 2,
      slidesToShow: 2,
      dots: true,
      arrows: false,
      mobileFirst: true,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToScroll: 4,
            slidesToShow: 4,
            dots: true,
            arrows: true
          }
        }
      ]
    }

    APP.i.general._registerSlickIntervalBind(() => {
      $(`.${classSlickShelf} > ul`).slick(slickOptions)
    })
  },

  addTargetBlank () {
    let banner = $('.home-small-banners .box-banner a')[3]
    $(banner).attr('target','_blank')
  },

  bind() { }
})
