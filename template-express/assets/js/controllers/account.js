APP.controller.Account = ClassAvanti.extend({
  init: function() {
    //this.setup()
    this.start()
    //this.bind()
  },

  setup: function() {
    
  },

  start: function() {
    this.receiveNewsletterChecked()
  },

  receiveNewsletterChecked: function() {
    $('#newsletterOptIn').click();
    const interval = setInterval(() => {
      if ($('#newsletterOptIn').length > 0) {
        clearInterval(interval)
        $('#newsletterOptIn').click();
      }
    }, 100)
  },

  bind: function() {
    
  }
})
