# VTEX CMS Generator (Using AVANTIGEN)
> A tool for auto generate VTEX CMS project structure.

## What is AVANTIGEN

Avantigen is a tool to be able to use Gulp for project scaffolding.

Avantigen does not contain anything "out of the box", except the ability to locate installed Avantigen generators
and to run them with [`liftoff`](https://www.npmjs.org/package/liftoff).


## Install AVANTIGEN

Install `Avantigen` globally with:

```bash
npm install -g avantigen
```


## Install AVANTI CMS Boilerplate

Install the `boilerplate` globally with:

```bash
npm install -g avantigen-vtex-boilerplate
```


## Usage

Navigate to your project directory and run:

```bash
avantigen vtex-boilerplate
```

You need tell what is the project name and the acronym. Ex: Beagle > bgl
Finally, select if you need to import vendors.